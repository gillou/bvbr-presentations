+++
title = "Bien vivre en Bretagne Romantique"
description = "Présentation et utilisation de la boutique du marché ambulant"
outputs = ["Reveal"]
[logo]
src = "logo-bvbr.png"
alt = "BVBR"
[reveal_hugo]
custom_theme = "reveal-hugo/themes/robot-lung.css"
margin = 0.1
highlight_theme = "color-brewer"
# Régler auto_slide pour un diaporama automatique
# valeur en millisecondes, 0 pour défilement manuel
auto_slide = 0
loop = false
transition = "slide"
transition_speed = "fast"
+++

## [Le Marché Ambulant](https://www.bvbr.org/marche-ambulant/)

### Explications de la boutique pour les producteurs / vendeurs


- [Création du compte](#/1)
- [Ajout des produits](#/2)
- [Export des commandes](#/3)

---

### Créer votre compte producteur sur le site du marché ambulant

1. Prendre contact avec l'association : admin@bvbr.org
2. Attendre le mail d'explications
3. Suivre le lien : créer la boutique
4. Attendre la validation du compte (24h max)
5. Ajouter des produits

---

### La checklist "Ajout produit Simple"

1. Choix du <span class="vert">type</span> de produits : simple ou variable
   - Vos produits sont bien réels (ne pas cocher virtuel)
2. Rentrer une <span class="vert">description</span> du produit
3. Ajouter une image <span class="rouge">carrée</span>
4. Renseigner les <span class="vert">catégories</span> et <span class="vert">étiquettes</span>
   - Utilisez uniquement les choix <span class="rouge">existants</span>
5. Gérer les <span class="vert">attributs</span>
   - PRODUCTEURS : Cocher & chercher votre nom
   - TYPE DE PRODUIT : Cocher & choisir (Frais, sec, ...)
6. Gérer les <span class="vert">stocks</span> dans l'inventaire (si besoin)

---


### C'est quoi un produit variable

<ul>
    <li class="fragment">C'est un produit avec des <span class="vert">variantes</span></li>
    <ul><li class="fragment">Poids, Format, ...</li></ul>
    <li class="fragment">Choix éditorial</li>
    <ul><li class="fragment">Plusieurs produits simples = 1 produit avec des variations</li></ul>
    <li class="fragment">Souvent, un seul attribut varie</li>
    <li class="fragment">Inspirez vous des autres marchands</li>
</ul>


---

### La checklist "Ajout produit Variable"

1. → 6. suivre la checklist produit simple
7. Ajouter un <span class="vert">attribut</span> par variation (poids, format, ...)
8. Renseigner les produits dans <span class="vert">Variations</span>


#### 

<h4 class="fragment vert">→ C'est à vous !</h6>

---

<h2 class="rouge">Merci pour vos bons produits</h5>

{{< tenor url="11649302" ratio="1.7849" >}}

---

## Récupérer vos commandes 

### 3 possibilités

<ol>
    <li class="fragment">Export des produits commandés, PDF, Excel</li>
    <li class="fragment">Liste des articles par commande client : <span class="fragment" style="color:#FF8d00;">Packing Slip / Bordereau d'envoi</span></li>
    <li class="fragment">Quantités commandées par produit</li>
</ul>

<h6 class="fragment" style="color:#FF8d00;">→ On est encore en phase de mise en route</h6>

---

#### 1. Export des produits commandés


![Accès aux commandes](Store-Manager-commandes-column.jpg#right)

<ul>
    <li class="fragment">Allez sur votre page vendeur</li>
    <li class="fragment">Cliquez dans le menu <span style="color:#FF8d00;">"Commandes"</span> </li>
    <li class="fragment">Choisissez le format d'export : PDF, Excel, ...</li>
</ul>

<h4 class="fragment" style="color:#1aae4b;">→ Attention à la pagination</h6>
<p class="fragment">Un PDF = 25 commandes max</p>

---

#### 2. Export des commandes clients : <span style="color:#FF8d00;">Packing Slip / Bordereau d'envoi</span>

<ul>
    <li class="fragment">Affichez les commandes</li>
    <li class="fragment">Pour chaque ligne cherchez l'icône <span style="color:#FF8d00;">[P]</span>, colonne de droite</li>
    <li class="fragment">Cliquez dessus pour obtenir le PDF de l'intégrale de la commande client</li>
</ul>

<img class="half" data-src="Store-Manager-packing-slip.png">

---

#### 3. Quantités commandées par produit

<ul>
    <li class="fragment">Allez dans le menu <span style="color:#FF8d00;">"Rapports"</span> </li>
    <li class="fragment">Pour chaque ligne cherchez l'icône [P], colonne de droite</li>
    <li class="fragment">Cliquez dessus pour obtenir le PDF de l'intégrale de la commande client</li>
</ul>

<img class="half" data-src="Store-Manager-achats-produits.png">

---

<h2 class="rouge">Merci</h2>

<h5 class="vert">Vous êtes héroïques</h5>

{{< tenor url="5571475" ratio="1.83" >}}
