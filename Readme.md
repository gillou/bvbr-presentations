# Supports des présentation d'utilisation du site de [bvbr.org](https://bvbr.org)

Auteur : Gilles Gonon

Site réalisé avec [Hugo](https://gohugo.io) et le thème [Reveal-js](https://themes.gohugo.io/theme/hugo-theme-revealjs/#/)

Publié sur les pages gitlab : [https://gillou.gitlab.io/bvbr-presentations/](https://gillou.gitlab.io/bvbr-presentations/)

## Installation 

- Cloner/forker le dépôt 
- Récupérer le thème Hugo associé Reveal-hugo, qui est un sous-module de ce dépôt. 

```git
git submodule update --init --recursive
```

- Installer [Hugo](https://gohugo.io)

## Lancement 

```bash
hugo server
```

Le site "_vit_" alors en local (live server), l'adresse à ouvrir est indiqué par la commande hugo dans la trace console, typiquement http://localhost:1313 (le port peut varier).

## Modifications

Aller dans le menu content à la racine du site et compléter le fichier _index.md.

Inspirez vous des fichiers existants ou du site example du thème revealjs